# @summary Main class, ensures dependencies are installed.
#
# Ensures all the yapgp module's dependencies are installed.
#
# @example
#   include yapgp
class yapgp {
  ensure_packages(['ruby-gpgme','gpg'])
}
