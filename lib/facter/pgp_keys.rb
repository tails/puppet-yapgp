# frozen_string_literal: true

Facter.add(:pgp_keys) do
  setcode do
    require 'gpgme'
    ctx = GPGME::Ctx.new({ protocol: GPGME::PROTOCOL_OpenPGP, keylist_mode: GPGME::KEYLIST_MODE_LOCAL })
    pgpkeys = {}
    ctx.keys.each do |key| # rubocop:disable Style/HashEachMethods
      pgpkeys[key.fingerprint] = {
        name: key.name,
        email: key.email,
        comment: key.comment,
        expires: key.expires,
        trust: key.owner_trust + 1,
        export: key.export(armor: true).to_s
      }
    end
    pgpkeys
  end
end
