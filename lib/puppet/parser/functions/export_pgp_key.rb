module Puppet::Parser::Functions
  newfunction(:export_pgp_key, type: :rvalue, doc: <<-DOC
    @summary
      Returns an armored export of the given key.

    Takes the key fingerprint as first argument.
    The second argument, the username to whom the keyring belongs, is optional.

    @return [Variant[String,Boolean]]
      Returns an armored key as string or `false` if the given key cannot be exported.
    DOC
  ) do |args|
    unless Puppet.features.gpgme?
      warning 'GPGME library not found, unable to export'
      return false
    end

    require 'gpgme'

    name = args[0]
    user = args[1]

    unless %r{\A(0x)?[0-9a-fA-F]{40}\Z}.match?(name)
      raise Puppet::Error, 'Key id must be a full 40-digit fingerprint.'
    end
    id = if name.start_with?('0x')
           name.partition('0x').last.upcase
         else
           name.upcase
         end

    if user && Etc.getpwnam(user).uid != Process.euid
      begin
        Process::GID.grant_privilege(Etc.getpwnam(user).gid)
        Process::UID.grant_privilege(Etc.getpwnam(user).uid)
      rescue
        raise Puppet::Error, "Unable to run as user #{user}"
      end
      ENV['HOME'] = Etc.getpwuid(Process.euid)['dir']
    end

    ctx = GPGME::Ctx.new({ protocol: GPGME::PROTOCOL_OpenPGP, keylist_mode: GPGME::KEYLIST_MODE_LOCAL })
    keys = ctx.keys(id)
    if keys.empty?
      warning "No key #{id} found, unable to export"
      return false
    end
    key = keys[0]
    exp = key.export(armor: true).to_s
    if user && Process.uid != Process.euid
      Process::UID.switch
      Process::GID.switch
      ENV['HOME'] = Etc.getpwuid(Process.uid)['dir']
    end
    return exp
  end
end
