# frozen_string_literal: true

require 'gpgme' if Puppet.features.gpgme?
require 'open-uri'

Puppet::Type.type(:pgp_key).provide(:pgp_key) do
  desc 'pgp_key provider for pgp_key resource'

  confine feature: :gpgme

  def exists?
    drop_to_user
    ctx = GPGME::Ctx.new({ protocol: GPGME::PROTOCOL_OpenPGP, keylist_mode: GPGME::KEYLIST_MODE_LOCAL })
    keys = ctx.keys(resource[:fp])
    revert_privs
    if keys.empty?
      return false
    end
    if resource[:ensure] == :absent
      return true
    end
    key = keys[0]
    if key.primary_subkey.expired
      return false
    end
    actual_trust = if key.owner_trust == 0
                     1
                   else
                     key.owner_trust
                   end
    actual_trust == resource[:trust]
  end

  def create
    drop_to_user
    ctx = GPGME::Ctx.new({ protocol: GPGME::PROTOCOL_OpenPGP, keylist_mode: GPGME::KEYLIST_MODE_EXTERN })
    begin
      thiskey = ctx.get_key(resource[:fp])
      GPGME.gpgme_op_import_keys(ctx, [thiskey])
      tstctx = GPGME::Ctx.new({ protocol: GPGME::PROTOCOL_OpenPGP, keylist_mode: GPGME::KEYLIST_MODE_LOCAL })
      if tstctx.keys(resource[:fp]).empty?
        raise EOFError
      end
    rescue EOFError
      raise Puppet::Error, "Unable to retrieve key #{resource[:fp]}, please provide a source" unless resource[:source]
      thiskeydata = GPGME::Data.new(source_to_string(resource[:source]))
      ctx.import_keys(thiskeydata)
      tstctx = GPGME::Ctx.new({ protocol: GPGME::PROTOCOL_OpenPGP, keylist_mode: GPGME::KEYLIST_MODE_LOCAL })
      if tstctx.keys(resource[:fp]).empty?
        raise Puppet::Error, "Unable to import key from source #{resource[:source]}."
      end
    end
    revert_privs
    trust = resource[:trust] + 1
    command = '/bin/echo "' + resource[:fp] + ':' + trust.to_s + ':" | /usr/bin/gpg --import-ownertrust'
    begin
      Puppet::Util::Execution.execute(command, uid: user_id, gid: group_id)
    rescue Puppet::ExecutionFailure
      raise Puppet::Error, "Could not set trust level for key #{resource[:fp]}"
    end
  end

  def destroy
    drop_to_user
    ctx = GPGME::Ctx.new({ protocol: GPGME::PROTOCOL_OpenPGP, keylist_mode: GPGME::KEYLIST_MODE_LOCAL })
    thiskey = ctx.get_key(resource[:fp])
    GPGME.gpgme_op_delete(ctx, thiskey, 1)
    revert_privs
  end

  def drop_to_user
    return if Process.euid == user_id
    begin
      Process::GID.grant_privilege(group_id)
      Process::UID.grant_privilege(user_id)
    rescue
      raise Puppet::Error, "Unable to run as user #{resource[:user]}"
    end
    ENV['HOME'] = Etc.getpwuid(Process.euid)['dir']
  end

  def revert_privs
    return if Process.euid == Process.uid
    Process::UID.switch
    Process::GID.switch
    ENV['HOME'] = Etc.getpwuid(Process.uid)['dir']
  end

  def user_id
    Etc.getpwnam(resource[:user]).uid
  end

  def group_id
    Etc.getpwnam(resource[:user]).gid
  end

  def source_to_string(value)
    parsed_value = URI.parse(value)
    if parsed_value.scheme.nil?
      raise(_('The file %{_value} does not exist') % { _value: value }) unless File.exist?(value)
      f = File.open(value, 'r')
      key = f.read
      f.close
      key
    else
      begin
        # Only send basic auth if URL contains userinfo
        # Some webservers (e.g. Amazon S3) return code 400 if empty basic auth is sent
        if parsed_value.userinfo.nil?
          key = parsed_value.read
        else
          user_pass = parsed_value.userinfo.split(':')
          parsed_value.userinfo = ''
          key = open(parsed_value, http_basic_authentication: user_pass).read
        end
      rescue OpenURI::HTTPError, Net::FTPPermError => e
        raise(_('%{_e} for %{_resource}') % { _e: e.message, _resource: resource[:source] })
      rescue SocketError
        raise(_('could not resolve %{_resource}') % { _resource: resource[:source] })
      else
        key
      end
    end
  end

  mk_resource_methods
end
