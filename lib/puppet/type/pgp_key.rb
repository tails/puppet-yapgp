# frozen_string_literal: true

Puppet::Type.newtype(:pgp_key) do
  @doc = <<-MANIFEST
    @summary This type provides Puppet with the capability to manage PGP keys

    @example Basic usage
      pgp_key { 'Tails sysadmins':
        fp     => 'D113CB6D5131D34BA5F0FE9E70F4F03116525F43',
        ensure => present,
        trust  => 4,
        user   => root,
      }
  MANIFEST

  autorequire(:class) { 'yapgp' }

  ensurable do
    desc 'Ensure the key is present or absent'
    defaultto :present
    newvalue(:present) do
      provider.create
    end
    newvalue(:absent) do
      provider.destroy
    end
  end

  newparam(:name, namevar: true) do
    desc 'The unique name of this resource.'
  end

  newparam(:fp) do
    desc 'The fingerprint of the key you want to manage, should be the full 40-digit key fingerprint.'
    newvalues(%r{\A(0x)?[0-9a-fA-F]{40}\Z})
    munge do |value|
      fp = if value.start_with?('0x')
             value.partition('0x').last.upcase
           else
             value.upcase
           end
      fp
    end
  end

  newparam(:trust) do
    desc 'The trustlevel of the key you want to manage, should be an integer between 1 and 5.'
    newvalues(%r{\A[1-5]\Z})

    defaultto 1
  end

  newparam(:user) do
    desc 'The user for whom you want to manage the key.'

    defaultto 'root'
  end

  newparam(:source) do
    desc 'Where to retrieve the key from (URI or local file)'
  end
end
