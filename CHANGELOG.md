# Changelog

All notable changes to this project will be documented in this file.

## Release 0.1.3

**Features**

  - added source parameter to `pgp_key` to allow importing from file or URI.

## Release 0.1.2

**Features**

  - Added pgp_keys custom fact

## Release 0.1.1

  - Moved to different repository

## Release 0.1.0

**Features**

  - The type `pgp_key`, which ensures a pgp key is up to date and at the right trustlevel.
  - The function `export_pgp_key`, which returns the armored public key.

**Bugfixes**

**Known Issues**
