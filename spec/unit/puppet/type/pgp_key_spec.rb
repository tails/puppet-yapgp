# frozen_string_literal: true

require 'spec_helper'
require 'puppet/type/pgp_key'

RSpec.describe 'the pgp_key type' do
  it 'loads' do
    expect(Puppet::Type.type(:pgp_key)).not_to be_nil
  end
end
