# yapgp

Yet another PGP puppet module.

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with yapgp](#setup)
    * [Beginning with yapgp](#beginning-with-yapgp)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module is designed to manage PGP public keys. It can fetch keys, keep
them up to date, set the trust level, and export them.

## Setup

### Beginning with yapgp

Just `include yapgp` and you're good to go (this will install ruby-gpgme).

## Usage

Typical usage would look like:

```
include yapgp

pgp_key { 'Tails sysadmins':
  fp     => 'D113CB6D5131D34BA5F0FE9E70F4F03116525F43':
  ensure => present,
  user   => root,
  trust  => 4,
}
```

This will add the sysadmins@tails.net PGP public key to root's keyring
and set the owner trustlevel to 4.

In case the key is not retrievable from external sources (e.g., keyservers)
or not importable from external sources (for instance, when a uid is missing),
you may provide a `source` parameter:

```
pgp_key { 'deb.tails.boum.org signing key':
  ensure => present,
  source => 'https://gitlab.tails.boum.org/tails/tails/-/raw/stable/config/chroot_sources/tails.chroot.gpg?inline=false',
  fp     => 'D68F87149EBA77541573C1C12453AA9CE4123A9A',
}
```
Upon failing to import the key from external sources, yapgp will then attempt
to fetch the keydata from the provided source (a URI or file location).

### Functions

To export a key, you can use the export_pgp_key function. There are two caveats
to keep in mind. First, by default, functions run on the puppet server. However,
since gpgme uses native C extensions, it won't run in puppet server's jruby
environment. Luckily, gpgme runs fine on the local agent, so be sure to use
Deferred to ensure the function is run on the agent and not the server.
Secondly, keep in mind that puppet functions are ran during catalog compilation,
so regardless of the relationship you specify, it will always run *before* any
pgp_key types are executed. Hence, to prevent exporting keys that are not
present in the keyring (yet), it is recommended to set a condition like this:

```
if Deferred('export_pgp_key', ['D113CB6D5131D34BA5F0FE9E70F4F03116525F43']) {
  file { '/root/tails-sysadmins.asc':
    content => String(Deferred('export_pgp_key', ['D113CB6D5131D34BA5F0FE9E70F4F03116525F43'])),
  }
}
```

### Facts

This module adds a custom fact called `pgp_keys`, containing a hash of
all keys available in root's keychain, indexed by fingerprint.

## Limitations

This module assumes dirmngr is properly configured.

Only Debian 10 and higher are currently supported.

There is no support for dealing with private keys, nor for any encryption
or decryption of data.

## Development

Merge requests are welcome ;-)
